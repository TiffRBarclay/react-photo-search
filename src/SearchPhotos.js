import React, { useState } from "react";
import CameraAltIcon from "@material-ui/icons/CameraAlt";

function SearchPhotos() {
  const [query, setQuery] = useState("");
  return (
    <>
      <form className="form">
        <label className="label" htmlFor="query">
          {" "}
          <CameraAltIcon style={{ fontSize: "35px" }} />
        </label>
        <input
          type="text"
          name="query"
          className="input"
          placeholder={`Try "dog" or "apple"`}
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <buton type="submit" className="button">
          Search
        </buton>
      </form>
    </>
  );
}

export default SearchPhotos;
